//
//  ContentView.swift
//  SwiftUIDemoCloudKit
//
//  Created by apple on 20/08/19.
//  Copyright © 2019 Emxcel. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
